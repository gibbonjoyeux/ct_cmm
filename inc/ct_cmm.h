
#ifndef CT_CMM_H
# define CT_CMM_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

# include <inttypes.h>
# include <stdlib.h>

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// REPLACE
//////////////////////////////////////////////////

//////////////////////////////
/// BASIC CONDITION
//////////////////////////////

# define if						if (
# define then					) {
# define elif					} else if (
# define else					} else {

# define while					while (
# define for					for (
# define do						) {

# define end					}

//////////////////////////////
/// MATCH (SWITCH)
//////////////////////////////

# define match					switch (
# define in						) {
# define O						case
# define X						default
# define break					break;

//////////////////////////////
/// OPERATION
//////////////////////////////

# define or						||
# define and					&&

//////////////////////////////////////////////////
/// MACRO
//////////////////////////////////////////////////

//////////////////////////////
/// OPERATIONS
//////////////////////////////

# define ABS(x)				(((x) < 0) ? -(x): (x))
# define MAX(x, y)			(((x) > (y)) ? (x) : (y))
# define MIN(x, y)			(((x) > (y)) ? (y) : (x))
# define SIGN(x)			(((x) < 0) ? -1 : (((x) > 0) ? 1 : 0))
# define ROUND_DIV(x, y)	((x) / (y) + (((x) % (y)) ? 1 : 0))

# define IS_ODD(x)			((x) & 1)
# define IS_EVEN(x)			(!ODD(x))

# define BIT_GET(x, y)		((uint8_t)(((x) >> (y)) & 1))
# define BIT_SET(x, y)		((x) | ((uint64_t)1 << (y)))
# define BIT_LET(x, y)		((x) & (UINT64_MAX ^ ((uint64_t)1 << (y))))
# define BIT_REV(x, y)		((x) ^ ((uint64_t)1 << (y)))

# define range(i, x, y)		i = (x); i < (y); ++i
# define range(i, x, y, z)	i = (x); i != (y); i += (z)

//////////////////////////////
/// BOOL
//////////////////////////////

# define true				1
# define false				0

////////////////////////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////////////////////////

typedef int8_t					i8;
typedef int16_t					i16;
typedef int32_t					i32;
typedef int64_t					i64;
typedef uint8_t					u8;
typedef uint16_t				u16;
typedef uint32_t				u32;
typedef uint64_t				u64;

typedef uint8_t					byte;
typedef size_t					size;

typedef void*					addr;
typedef uint8_t*				addr8;
typedef uint16_t*				addr16;
typedef uint32_t*				addr32;
typedef uint64_t*				addr64;

typedef uint8_t					bitset8;
typedef uint16_t				bitset16;
typedef uint32_t				bitset32;
typedef uint64_t				bitset64;

typedef enum e_all_enum			t_all_enum;
typedef union u_all_value		t_all_value;

////////////////////////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// PRIVATE
//////////////////////////////////////////////////

enum			e_all_enum
{
	I8,
	I16,
	I32,
	I64,
	U8,
	U16,
	U32,
	U64,
	BYTE,
	SIZE,
	ADDR,
	ADDR8,
	ADDR16,
	ADDR32,
	ADDR64,
	BITSET
};

union			u_all_value
{
	i8			i8;
	i16			i16;
	i32			i32;
	i64			i64;
	u8			u8;
	u16			u16;
	u32			u32;
	u64			u64;
	byte		byte;
	size		size;
	addr		addr;
	addr8		addr8;
	addr16		addr16;
	addr32		addr32;
	addr64		addr64;
	bitset64	bitset;
};

//////////////////////////////////////////////////
/// PUBLIC
//////////////////////////////////////////////////

struct			s_all
{
	t_all_enum	type;
	t_all_value	value;
};

#endif

